import logging
import socket
import threading
import timeit

HEADER = 64
PORT = 64511
SERVER = 'localhost'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "DISCONNECTED!"


clients = []
nickname = []

# create formatter
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.DEBUG)

# create logger
logger = logging.getLogger('[SEARCH]')
logger.setLevel(logging.DEBUG)


class TcpConnect:

    def __init__(self, sock=None):

        if sock is None:
            # server using IPV4 and TCP socket
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        else:
            self.sock = sock
        # bind server to port number and Ip adderess
        self.sock.bind(ADDR)
        self.sock.listen()
        self.msg_recv = ""
        self.search_result = ""
        print('[STARTING] SERVER IS STARTING ....')

    def closeConnect(self):
        """
        Close the connection from the client
        """
        self.sock.close()

    def checkForString(self, string, path):
        """
        Check if string exist or not

        This is a function search for a string exist in a file or not

        Parameter
        --------

        string: str
            string to search for in the file
        path: str
            Path containing the file location

        Returns
        -------
        str:
            "STRING EXISTS" - if the string is in the file
            "STRING NOT FOUND" - if the string is not in the file
        """
        try:
            # open file in read mode
            with open(path, "rb") as f:
                # read everyline from the file
                lines = f.readlines()
            # loop through each line
            for line in lines:
                line = line.decode('utf-8')
                # check if the file contain the sting
                if string in line:
                    return "STRING EXISTS"
            return "STRING NOT FOUND"
        except Exception:
            return "STRING NOT FOUND"

    def findPath(self):
        """
        find linux path from the configuration file

        Parameter
        --------

        Returns:
        -------
        str: path to the file location

        Examples
        --------
        >>> findPath()
        linuxpath=/root/200k.txt
        """
        # open file in read mode
        with open('searchconfig.ini', "rb") as f:
            # read everyline from the file
            lines = f.readlines()
            # loop through each line
            for line in lines:
                line = line.decode('utf-8')
                # check if a line starts with "linuxpath="
                check = line.startswith(('linuxpath='))
                if check:
                    return line

    def splitPath(self, line):
        """
        split linux path

        Parameter
        --------
        str: path of the linux file which will be checked
            if it contain the string or not

        Returns:
        -------
        str: separated path to the file location

        Examples
        --------
        >>> split(linuxpath=/root/200k.txt)
        /root/200k.txt
        >>> split(linuxpath=/root/400k.txt)
        /root/400k.txt
        """
        # split the path from the prefix
        paths = line.split('=')
        path = paths[-1]
        return path

    def handle_client(self, conn, addr):
        """
        Handels clients request and sends response to the client

        Parameter
        --------

        conn: object
            an object used to reciev or send data from the client
        addr: object
            Contain IP address and Port of the client connected

        Returns:
        -------
        str:
            "STRING EXISTS" - if the string is in the file
            "STRING NOT FOUND" - if the string is not in the file
        """
        self.client, self.address = conn, addr
        # recive msg from the client and strip (\x00)
        self.msg_recv = self.client.recv(1024).decode(FORMAT).rstrip('\x00')
        # call findPath method
        paths = self.findPath()
        # call splitPath method
        path = self.splitPath(paths)
        # call checkForString method
        self.search_result = self.checkForString(self.msg_recv, path)
        self.client.send(self.search_result.encode('utf-8'))
        return True


if __name__ == '__main__':
    serv = TcpConnect()
    while True:
        conn, addr = serv.sock.accept()
        starttime = timeit.default_timer()
        thread = threading.Thread(target=serv.handle_client, args=(conn, addr))
        thread.start()
        exec_msg = 'execution time:'
        exec_time = timeit.default_timer() - starttime
        ip = 'IP:' + addr[0]
        port = 'Port:' + str(addr[1])
        time = round(exec_time * 1000, 1)
        logger.debug(f'''{exec_msg} {time}ms {ip} {port} {serv.msg_recv}''')

