import socket
from unittest import TestCase

import mock

from .search import TcpConnect

PORT = 64511
SERVER = '127.0.0.1'
FORMAT = 'utf-8'


class TCPSocketConnectionTest(TestCase):
    def setUp(self):
        ''' setup connection for server and clinet'''
        self.connection = TcpConnect()
        self.mock_socket = mock.Mock()
        self.mock_socket.recv.return_value = "data 1234"
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client.connect((SERVER, PORT))
        self.client.send("Thanks".encode(FORMAT))

    def tearDown(self):
        '''close created connection'''
        self.connection.closeConnect()
        self.client.close()

    def test_gethostname(self):
        hostname = socket.gethostname()
        self.mock_socket.disable()
        self.assertEqual(socket.gethostname(), hostname)

    def test_findPath(self):
        '''test the findpath method find the line start with linuxpath='''
        line = self.connection.findPath()
        self.assertTrue(line.startswith(("linuxpath=")))

    def test_splitPath(self):
        '''test the splitpath method returns the path only'''
        path = self.connection.splitPath('linuxpath=/root/200k.txt')
        self.assertEqual(path, "/root/200k.txt")

    def test_checkForString(self):
        '''test if the string exitst or not'''
        check = self.connection.checkForString(
            '13;0;1;26;0;7;3;0;',
            '/home/nathy/algo_test/mock_socket/200k.txt')
        self.assertEqual(check, "STRING EXISTS")

    def test_checkForString_not(self):
        '''test for Permission to read the file'''
        with mock.patch('__main__.open') as mock_oserror:
            mock_oserror.side_effect = PermissionError
            try:
                self.connection.checkForString(
                    'testfile2.txt',
                    '/home/nathy/algo_test/mock_socket/200k.txt')
            except SystemExit:
                print('test passed, sys.exit() called')

    def test_handle_client(self):
        client, address = self.connection.sock.accept()
        buf = self.connection.handle_client(client, address)
        self.assertTrue(buf)

