How to install Search server

step 1: update server
```
sudo apt-get update
sudo apt-get upgrade -y
```
step 2: install python
```
sudo apt install python3
```
step 3: clone the repo from
```
git clone https://gitlab.com/NathyG2524/search_server.git
```
step 4: open searchconfig.ini and specify the path to file
```
$ cd search_server
$ nano searchconfig.ini
```
step 5: look for the line start with 'linuxpath=' and add your path
```
...
[linux]
linuxpath=/example/path/to/file
....
````
step 6: run the following commands to add it to the service list
```
$ sudo cp server.service /etc/systemd/system/server.service
$ sudo systemctl enable /etc/systemd/system/server.service

```
step 7: make a new directory in usr/local/lib and place the program inside it
```
$ sudo mkdir /usr/local/lib/server
$ sudo cp search.py /usr/local/lib/server/server.py
```
So now we all are set we can start the server by
```
$ sudo systemctl daemon-reload
$ sudo service server start
````

To check for errors or not, you can run this command:
```
$ sudo service server status
```

To stop the script, you can run the command:
```
$ sudo service server stop
```

